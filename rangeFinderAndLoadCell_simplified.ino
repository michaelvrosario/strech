#define trigPin 7
#define echoPin 6
#define ledPin_1 9
#define ledPin_2 10
#define ledPin_3 11
#define ledPin_go 12
#define buttonPin 13
#define piezoPin 5
#define Fpin A0
#define n_data 500




void setup() {
  Serial.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(ledPin_1, OUTPUT);
  pinMode(ledPin_2, OUTPUT);
  pinMode(ledPin_3, OUTPUT);
  pinMode(ledPin_go, OUTPUT);
  pinMode(buttonPin, INPUT);
  
  
}

bool ready = true;

void loop() {
  // continually polling for button press
  // polling ends once calibration and data collection begins
  // polling continues after n_data points captured
  
  int button = digitalRead(buttonPin);
  if(ready){
    if(button == HIGH){
      ready = calibrate(); // returns false as a safety measure
      ready = captureData(); // returns true to allow button press to activate
    }
  }
}


float readDistance(){
  // safety measure, pull-down trigger pin prior to trigger
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2);

  // trigger ultrasonic pulses and turn off
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // listen for pulse reflection
  float distance = pulseIn(echoPin, HIGH);
  return(distance);
}


bool calibrate(){
  // THREE
  delay(500);
  digitalWrite(ledPin_1, HIGH);
  tone(piezoPin, 523, 250);
  delay(1000);

  // TWO
  digitalWrite(ledPin_1, LOW);
  digitalWrite(ledPin_2, HIGH);
  tone(piezoPin, 523, 250);
  delay(1000);
  
  // ONE
  digitalWrite(ledPin_2, LOW);
  digitalWrite(ledPin_3, HIGH);
  tone(piezoPin, 523, 250);
  delay(1000);

  // GO!!!!!
  digitalWrite(ledPin_3, LOW);
  tone(piezoPin, 1046, 500);
  return(false);
}

bool captureData(){
  // turn on LED while data recording
  digitalWrite(ledPin_go, HIGH);
  
  for(int i=0; i<n_data; i++){
    float x = readDistance();
    float F = analogRead(Fpin);
    Serial.print(i); // index value
    Serial.print(", ");
    Serial.print(x); // ultrasonic range finder
    Serial.print(", ");
    Serial.println(F); // force
  }

  // turn off LED after data collected
  digitalWrite(ledPin_go, LOW);
  return(true);
}


